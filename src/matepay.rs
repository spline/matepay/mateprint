// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use anyhow::anyhow;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tokio::sync::RwLock;
use uuid::Uuid;

use crate::config::{GeneralConfig, MatePayConfig};

pub struct MatePayService {
    matepay_config: MatePayConfig,
    general_config: GeneralConfig,
    client: Client,
    payments: RwLock<HashMap<Uuid, i32>>,
}

#[derive(Serialize)]
pub struct CreatePaymentRequest {
    pub product_ids: Vec<i32>,
    pub redirect_url: String,
}

#[derive(Deserialize)]
pub struct PaymentRequest {
    pub payment_request_id: i32,
    pub payment_url: String,
}

#[derive(Deserialize)]
struct PaymentStatus {
    completed: bool,
}

impl MatePayService {
    pub fn new(matepay_config: MatePayConfig, general_config: GeneralConfig) -> MatePayService {
        MatePayService {
            general_config,
            matepay_config,
            client: Client::new(),
            payments: RwLock::new(HashMap::new()),
        }
    }

    pub async fn create_payment(&self, uuid: &Uuid, pages: u32) -> anyhow::Result<PaymentRequest> {
        let request_id = uuid.to_string();
        let base_address = &self.general_config.address;
        let matepay_url = &self.matepay_config.url;

        let payment: PaymentRequest = self
            .client
            .post(format!(
                "{matepay_url}/api/v1/app_server/add_payment_request",
            ))
            .header("MATEPAY-APP-TOKEN", &self.matepay_config.service_token)
            .json(&CreatePaymentRequest {
                product_ids: std::iter::repeat(self.matepay_config.product_id)
                    .take(pages as usize)
                    .collect(),
                redirect_url: format!("{base_address}/confirm-print?print_request={request_id}",),
            })
            .send()
            .await?
            .json()
            .await?;

        self.payments
            .write()
            .await
            .insert(*uuid, payment.payment_request_id);

        Ok(payment)
    }

    pub async fn check_payment(&self, uuid: &Uuid) -> anyhow::Result<bool> {
        let read_lock = self.payments.read().await;
        let payment_id = read_lock
            .get(uuid)
            .ok_or(anyhow!("print request not known"))?;
        let matepay_url = &self.matepay_config.url;

        Ok(self
            .client
            .get(format!(
                "{matepay_url}/api/v1/app_server/payment/{payment_id}/status",
            ))
            .header("MATEPAY-APP-TOKEN", &self.matepay_config.service_token)
            .send()
            .await?
            .json::<PaymentStatus>()
            .await?
            .completed)
    }

    pub async fn remove_request(&self, uuid: &Uuid) -> anyhow::Result<()> {
        let payment_id = {
            let read_lock = self.payments.read().await;
            *read_lock
                .get(uuid)
                .ok_or(anyhow!("print request not known"))?
        };
        self.payments.write().await.remove(uuid);
        let matepay_url = &self.matepay_config.url;

        self.client
            .delete(format!(
                "{matepay_url}/api/v1/app_server/payment/{payment_id}",
            ))
            .header("MATEPAY-APP-TOKEN", &self.matepay_config.service_token)
            .send()
            .await?;
        Ok(())
    }

    pub async fn request_exists(&self, uuid: &Uuid) -> bool {
        self.payments.read().await.contains_key(uuid)
    }
}
