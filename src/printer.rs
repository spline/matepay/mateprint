// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::env;

use ipp::operation::builder::*;
use ipp::prelude::*;

use tokio::fs::File;
use tokio_util::compat::TokioAsyncReadCompatExt;

use crate::PrintOptions;

use log::info;

fn add_option_if(condition: bool, option: IppAttribute) -> Vec<IppAttribute> {
    if condition {
        vec![option]
    } else {
        Vec::new()
    }
}

pub struct Printer {
    client: AsyncIppClient,
}

impl Printer {
    pub fn new(name: String) -> anyhow::Result<Printer> {
        let uri: Uri = format!("http://localhost:631/printers/{name}").parse()?;

        Ok(Printer {
            client: AsyncIppClient::new(uri),
        })
    }

    pub async fn attributes(&self) -> anyhow::Result<IppAttributes> {
        let req = IppOperationBuilder::get_printer_attributes(self.client.uri().to_owned()).build();
        Ok(self.client.send(req).await?.attributes().clone())
    }

    pub async fn print(&self, path: &str, options: &PrintOptions) -> anyhow::Result<()> {
        let payload = IppPayload::new_async(File::open(path).await?.compat());

        use IppValue::*;

        let req = IppOperationBuilder::print_job(self.client.uri().to_owned(), payload)
            .user_name(env::var("USER").unwrap_or_else(|_| "noname".to_owned()))
            .job_title(path)
            .attributes(add_option_if(
                options.double_sided,
                IppAttribute::new("sides", "two-sided-long-edge".parse()?),
            ))
            .attribute(IppAttribute::new("copies", Integer(options.copies)))
            .attribute(IppAttribute::new("media", "A4".parse()?))
            .attribute(IppAttribute::new("fitplot", Boolean(true)))
            .build();

        let response = self.client.send(req).await?;

        info!("IPP status code: {}", response.header().status_code());

        let attrs = response
            .attributes()
            .groups_of(DelimiterTag::JobAttributes)
            .flat_map(|g| g.attributes().values());

        for attr in attrs {
            info!("{}: {}", attr.name(), attr.value());
        }
        Ok(())
    }
}
