// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::matepay::MatePayService;
use std::{fs, time::Duration};
use uuid::Uuid;

pub async fn cleanup(matepay: &MatePayService) -> anyhow::Result<()> {
    for file in fs::read_dir(crate::TEMP_DIR)? {
        let file = file?;
        let last_modified = file.metadata()?.modified()?;
        if last_modified.elapsed()? > Duration::from_secs(24 * 60 * 60) {
            // one day
            fs::remove_file(file.path())?;

            match Uuid::parse_str(&file.file_name().to_string_lossy()) {
                Ok(uuid) => {
                    matepay.remove_request(&uuid).await?;
                }
                Err(e) => {
                    return Err(anyhow::anyhow!(
                        "Unexpected file {:?} in temporary directory: {e}",
                        file.file_name()
                    ));
                }
            }
        }
    }

    Ok(())
}
