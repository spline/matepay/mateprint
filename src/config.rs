// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use serde::Deserialize;

#[derive(Deserialize)]
pub struct MatePayConfig {
    pub url: String,
    pub service_token: String,
    pub product_id: i32,
}

#[derive(Deserialize)]
pub struct PrinterConfig {
    pub printer_names: Vec<String>,
    pub default_printer_name: String,
}

#[derive(Deserialize)]
pub struct GeneralConfig {
    pub address: String,
}
