// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use maud::{html, Markup, DOCTYPE};
use pdf::file::FileOptions;
use rocket::{
    form::{Form, FromForm},
    fs::{FileServer, TempFile},
    get,
    http::{ContentType, Status},
    post,
    response::{status::Custom, Redirect},
    routes,
    tokio::sync::RwLock,
    uri, State,
};
use std::{collections::HashMap, fmt::Debug};
use uuid::Uuid;

use ipp::prelude::*;

use log::{error, info, warn};

mod cleanup;
mod config;
mod matepay;
mod printer;

use config::{GeneralConfig, MatePayConfig, PrinterConfig};
use matepay::MatePayService;
use printer::Printer;

pub const TEMP_DIR: &str = "/var/lib/mateprint/tmp/";

pub type ResultResponse<T> = Result<T, Custom<Markup>>;

pub fn handle_io_error<T: Debug>(error: T) -> Custom<Markup> {
    error!("Error: {:?}", error);
    error_page(
        Status::InternalServerError,
        Some("The error has been logged on the server side"),
    )
}

fn inherit_base_template(inner: Markup) -> Markup {
    html! {
        (DOCTYPE)
        html {
            head {
                link href="/static/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous";
                meta name="viewport" content="width=device-width, initial-scale=1";
                title { "MatePrint" }
            }
            body class="p-3 m-0 border-0" {
                div class="container" style="max-width: 40rem; margin: 0 auto;" {
                    div class="card p-4 container justify-content-center" {
                        (inner)
                    }

                    div class="mt-3" style="float: right" {
                        a class="link-secondary p-2" href="https://gitlab.spline.inf.fu-berlin.de/spline/matepay/mateprint" { "Source Code" }
                        a class="link-secondary p-2" href="https://uptime.ghsq.de/status/fsi-info" { "System Status" }
                    }
                }
                script src="/static/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous" {}
            }
        }
    }
}

#[get("/")]
async fn default_print_form(printer_config: &State<PrinterConfig>) -> Redirect {
    Redirect::to(uri!(print_form(&printer_config.default_printer_name)))
}

fn printer_name_valid(allowed_names: &[String], name: &String) -> bool {
    if !name
        .chars()
        .all(|c| matches!(c, 'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '-' | '.'))
    {
        warn!("Printer name {:?} not allowed, because it contains characters not in 'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '-' | '.'", name);
        return false;
    }
    allowed_names.contains(name)
}

#[get("/<printer_name>")]
async fn print_form(
    printer_config: &State<PrinterConfig>,
    printer_name: String,
) -> ResultResponse<Markup> {
    if !printer_name_valid(&printer_config.printer_names, &printer_name) {
        return Ok(inherit_base_template(html! {
            h1 { "Requested printer not found" }

            p {
                "Please check the link you entered."
            }
        }));
    }

    // Detect printer attributes
    let printer = Printer::new(printer_name.clone()).map_err(handle_io_error)?;
    let attributes = printer.attributes().await.map_err(handle_io_error)?;

    let attrs = attributes
        .groups_of(DelimiterTag::PrinterAttributes)
        .flat_map(|g| g.attributes())
        .collect::<HashMap<_, _>>();

    let supported_job_attributes: Vec<String> = attrs
        .get(&"job-creation-attributes-supported".to_owned())
        .and_then(|a| a.value().as_array())
        .map(|a| a.iter().map(|v| v.to_string()).collect())
        .unwrap_or_default();

    let supported_sides: Vec<String> = attrs
        .get(&"sides-supported".to_owned())
        .and_then(|a| a.value().as_array())
        .map(|a| a.iter().map(|v| v.to_string()).collect())
        .unwrap_or_default();

    let double_sided_supported = supported_job_attributes.contains(&"sides".to_owned())
        && supported_sides.contains(&"two-sided-long-edge".to_owned());
    let copies_supported = supported_job_attributes.contains(&"copies".to_owned());

    Ok(inherit_base_template(html! {
        h1 {
            "Print"

            @if printer_config.printer_names.len() > 1 {
                div class="dropdown" style="float: right" {
                    button class="btn btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false" {
                        (printer_name)
                    }

                    // printer selection
                    ul class="dropdown-menu" {
                        @for printer in &printer_config.printer_names {
                            li {
                                a href=(uri!(print_form(printer))) class="dropdown-item" { (printer) }
                            }
                        }
                    }
                }
            }
        }

        h2 { "Upload a document" }
        p {
            "Documents can be uploaded as PDF files.
            Before printing, make sure the PDF document contains pages in A4 format, as otherwise it will be up to the system to decide how to layout them."
        }

        h2 { "Payment" }
        p {
            "We currently bill 2 cent per page. This is just in order to buy new toner if needed. The payment works through MatePay, and will be done before starting to print."
        }

        p {
            "Before continuing, make sure you have a Spline account. You can " a href="https://accounts.spline.de/register" { "register one here" } "."
        }

        p {
            "Please note that we currently can not guarantee that printing always works. In case it didn't, you can get your money back in cash in the FSI room."
        }

        form action="/print" method="POST" enctype="multipart/form-data" class="mt-3" {
            input type="file" accept="application/pdf" id="file" name="file" class="form-control mb-3";

            input type="hidden" id="printer_name" name="options.printer_name" value=(printer_name);

            div class="accordion mb-3" id="options" {
                div class="accordion-item" {
                    h2 class="accordion-header" id="heading" {
                        button class="accordion-button collapsed" style="height: 37px" type="button" data-bs-toggle="collapse" data-bs-target="#collapse" aria-expanded="false" aria-controls="collapse" {
                            "Options"
                        }
                    }
                    div id="collapse" class="accordion-collapse collapse" aria-labelledby="heading" data-bs-parent="#options" {
                        div class="accordion-body" {
                            // Double sided printing
                            @if double_sided_supported {
                                div class="form-check mb-3" {
                                    label for="double_sided" class="form-check-label" { "Print Double Sided" }
                                    input type="checkbox" id="double_sided" name="options.double_sided" class="form-check-input";
                                }
                            } @else {
                                input type="hidden" id="double_sided" name="options.double_sided" value="off";
                            }

                            // Copies
                            @if copies_supported {
                                label for="copies" class="form-label" { "Copies" }
                                input type="number" id="copies" name="options.copies" value="1" min="1" class="form-control mb-3";
                            } @else {
                                input type="hidden" id="copies" name="options.copies" value="1";
                            }
                        }
                    }
                }
            }

            input type="submit" value="Print" class="btn btn-primary" style="float: right";
        }
    }))
}

#[derive(FromForm)]
pub struct PrintOptions {
    printer_name: String,
    double_sided: bool,
    copies: i32,
}

#[derive(FromForm)]
struct Print<'f> {
    file: TempFile<'f>,
    options: PrintOptions,
}

fn request_uuid() -> Uuid {
    Uuid::new_v4()
}

fn file_path(print_request: &Uuid) -> String {
    format!("{TEMP_DIR}/{print_request}")
}

#[get("/notapdf?<printer_name>")]
async fn notapdf(printer_name: String) -> Markup {
    inherit_base_template(html! {
        p {
            "The chosen file was not a PDF document, please try again"
        }

        a class="btn btn-primary" href=(uri!(print_form(printer_name))) { "Try again" }
    })
}

type PrintOptionsMap = HashMap<Uuid, PrintOptions>;

fn error_page(code: Status, info: Option<&str>) -> Custom<Markup> {
    let title = code.reason().unwrap_or("Unknown Error");

    let body = inherit_base_template(html! {
        h1 { (title) }
        @if let Some(info) = info {
            p { (info) }
        }
    });

    Custom(code, body)
}

fn simple_error_page(code: Status) -> Custom<Markup> {
    error_page(code, None)
}

#[post("/print", data = "<form>")]
async fn print(
    service: &State<MatePayService>,
    printer_config: &State<PrinterConfig>,
    options: &State<RwLock<PrintOptionsMap>>,
    mut form: Form<Print<'_>>,
) -> ResultResponse<Redirect> {
    if let Err(e) = cleanup::cleanup(service).await {
        error!("Error while cleaning up: {e}");
    }

    let request_uuid = request_uuid();

    let path = file_path(&request_uuid);

    // Enforce PDF format
    let content_type = form
        .file
        .content_type()
        .ok_or(simple_error_page(Status::UnprocessableEntity))?;
    if *content_type != ContentType::PDF {
        info!("Got non-pdf document, refusing to process");
        return Ok(Redirect::to(uri!(notapdf(&form.options.printer_name))));
    }

    // Store document persistently
    form.file.persist_to(&path).await.map_err(handle_io_error)?;
    info!("Stored to {path}");

    if !printer_name_valid(&printer_config.printer_names, &form.options.printer_name) {
        return Err(simple_error_page(Status::UnprocessableEntity));
    }

    let copies = form.options.copies;

    options
        .write()
        .await
        .insert(request_uuid, form.into_inner().options);

    // Detect number of pages, will be used later for more exact payment.
    let pdf = FileOptions::cached().open(&path).map_err(handle_io_error)?;
    let pages: u32 = pdf.num_pages();
    info!("Got pdf with {pages} pages");

    // Create the payment request
    let payment = service
        .create_payment(&request_uuid, pages * copies.unsigned_abs())
        .await
        .map_err(handle_io_error)?;

    // Ask user to pay
    Ok(Redirect::to(payment.payment_url))
}

#[get("/confirm-print?<print_request>")]
async fn confirm_print(
    service: &State<MatePayService>,
    options: &State<RwLock<PrintOptionsMap>>,
    print_request: String,
) -> ResultResponse<Markup> {
    let print_request =
        Uuid::parse_str(&print_request).map_err(|_| simple_error_page(Status::NotFound))?;

    if !service.request_exists(&print_request).await {
        return Err(simple_error_page(Status::NotFound));
    }

    // Check if payment was accepted
    let completed = service
        .check_payment(&print_request)
        .await
        .map_err(handle_io_error)?;

    let path = file_path(&print_request);
    let options_lock = options.read().await;
    let print_options = options_lock
        .get(&print_request)
        .expect("Print request should have corresponding options");
    let printer_name = print_options.printer_name.to_string();
    let result = Ok(if completed {
        info!("Printing {path}…");
        match Printer::new(printer_name.to_string())
            .map_err(handle_io_error)?
            .print(&path, print_options)
            .await
        {
            Ok(()) => inherit_base_template(html! {
                h1 { "printing…" }

                h3 { "Troubleshooting" }
                ol {
                    li {
                        "If nothing happens, first try pressing the green start button on the printer"
                    }
                    li {
                        "Check whether the paper tray still has paper"
                    }
                    li {
                        "If the printer displays an error message, try turning it off and on again (for real)"
                    }
                }

                small class="mb-3" {
                    "In case problems persist, please notify "
                    a href="mailto:jbb@spline.de" { "jbb@spline.de" } "."
                }

                a href=(uri!(print_form(printer_name))) class="btn btn-primary" { "print more" }
            }),
            Err(e) => {
                info!("Failed to print: {e}");

                inherit_base_template(html! {
                    h1 { "Error" }
                    p {
                        "something went wrong and your page could not be printed :("
                    }

                    small class="mb-3" {
                        "In case problems persist, please notify "
                        a href="mailto:jbb@spline.de" { "jbb@spline.de" } "."
                    }
                })

                // TODO, once MatePay can do direct transfers, transfer the amount back.
            }
        }
    } else {
        inherit_base_template(html! {
            h1 { "Error" }
            p {
                "Something went wrong and your payment was not completed"
            }
        })
    });

    // Drop read lock, so we can aquire a write lock
    drop(options_lock);

    // Delete stored document
    info!("Deleting document");
    std::fs::remove_file(path).map_err(handle_io_error)?;
    options.write().await.remove(&print_request);
    service
        .remove_request(&print_request)
        .await
        .map_err(handle_io_error)?;

    result
}

#[rocket::launch]
async fn rocket() -> _ {
    pretty_env_logger::init();

    std::fs::create_dir_all(TEMP_DIR).expect("creating temporary directory");

    let rocket = rocket::build();

    let figment = rocket.figment();
    let matepay_config: MatePayConfig = figment
        .extract_inner("matepay")
        .expect("Reading matepay config");
    let printer_config: PrinterConfig = figment
        .extract_inner("printer")
        .expect("Reading printer config");
    let general_config: GeneralConfig = figment
        .extract_inner("general")
        .expect("Reading general config");

    rocket
        .manage(MatePayService::new(matepay_config, general_config))
        .manage(printer_config)
        .manage(RwLock::from(PrintOptionsMap::new()))
        .mount(
            "/",
            routes![
                default_print_form,
                print_form,
                print,
                confirm_print,
                notapdf
            ],
        )
        .mount("/static", FileServer::from("static"))
}
